
# Aamir Khan's Website

Hi There 🙋🏻‍♂️,

This is my website where you'll get to know me.

This is created using Hugo framework.



## Badges

Below are the badges:

[![MIT License](https://img.shields.io/badge/License-MIT-green.svg)](https://choosealicense.com/licenses/mit/)
[![Netlify Status](https://api.netlify.com/api/v1/badges/4176e3a8-6d90-4539-999e-42c0a3bf17bd/deploy-status)](https://app.netlify.com/sites/aamir363/deploys)